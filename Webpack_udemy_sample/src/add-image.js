import sunset from './sunset.jpeg';

function addImage() {
    const img = document.createElement('img');
    img.alt="sunset";
    img.width=200;
    img.src=sunset;

    const body = document.querySelector('body');
    body.appendChild(img);
}

export default addImage;