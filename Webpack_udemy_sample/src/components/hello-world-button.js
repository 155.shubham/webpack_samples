import './hello-world-button.css';

class HelloWorldButton {
    render() {
        const button = document.createElement('button');
        button.innerHTML='Hello World';
        button.classList.add('hello-world-button');
        const body = document.querySelector('body');
        button.onclick = function() {
            const para = document.createElement('p');
            para.innerHTML="Hello World Application";
            para.classList.add('hello-world-para');
            body.appendChild(para);
        };
        body.appendChild(button);
    };   
};

export default HelloWorldButton;