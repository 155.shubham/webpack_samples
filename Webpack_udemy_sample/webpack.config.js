const path = require('path');

module.exports = {
    entry:'./src/index.js',
    output: {
        filename:'bundle.js',
        path: path.resolve(__dirname,'./dist/'),
        publicPath:'dist/'
    },
    mode:'none',
    module: {
        rules:[
            {
                test: /\.(png|jpeg)$/,
                use:'file-loader'
            },
            {
                test: /\.css$/,
                use:['style-loader', 'css-loader']
            }
        ]
    }
}

//Real time Usage (production), following points to be noted down :-
//1. Change the public path from dist to the domain name. In this case the value for publicPath property will look like as below:
// publicPath: 'https://www.some-website.com/'